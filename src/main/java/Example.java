import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Command;
import java.io.File;
import Reader.ReaderAPI;
import sdk.Utility;

@Command(name = "example", mixinStandardHelpOptions = true, version = "Picocli example 4.0")
public class Example implements Runnable {

    @Option(names = { "-v", "--verbose" },
            description = "Verbose mode. Helpful for troubleshooting. Multiple -v options increase the verbosity.")
    private boolean[] verbose = new boolean[0];

    @Parameters(arity = "1..*", paramLabel = "FILE", description = "File(s) to process.")
    private File[] inputFiles;

    int res;
    int hScanner[] = new int[1];
    String strReaderIp = ""; //Ip of reader
    int m_Port = 1; //Port number
    int	m_HostPort = 2; //Host Port
    String strHostIp = "";
    int[] nBaudRate = new int[1];
    String strComm = "COM3";

    public void run() {
        res = ReaderAPI.ConnectScanner(hScanner, strComm, nBaudRate);
        System.out.println("aaa");
        if (verbose.length > 0) {
            System.out.println(inputFiles.length + " files to process...");
        }
        if (verbose.length > 1) {
            for (File f : inputFiles) {
                System.out.println(f.getAbsolutePath());
            }
        }
    }

    public void test() {
        int i = 0;
        int Address = 0;
        hScanner[0]	= 0;
        System.out.println("aaa");
        res = ReaderAPI.Net_ConnectScanner(hScanner, strReaderIp, m_Port, strHostIp, m_HostPort);
        res = ReaderAPI.ConnectScanner(hScanner, strComm, nBaudRate);
        // result 254
        System.out.println(res);
        System.out.println("bbb");
        if (res==ReaderAPI._OK) {
            //连上了，选择哪个模式通迅变恢
            short[] HandVer = new short[1];
            short[] SoftVer = new short[1];

            for (i = 0; i < 5; i++) {
                res = ReaderAPI.GetReaderVersion(hScanner[0], HandVer, SoftVer, Address);
                if (res == ReaderAPI._OK) {
                    break;
                }
            }
            if (res != ReaderAPI._OK) {
                //MessageBox("Connect Reader Fail!(Version)","Warning",MB_ICONWARNING);
                System.out.println("Connect Reader Fail!(Version)");
                //调关闭
                return;
            }
        } else {
            //连接失败
            System.out.println("Connect Reader Fail!");

        }
    }

    public static void main(String[] args) {
        // By implementing Runnable or Callable, parsing, error handling and handling user
        // requests for usage help or version help can be done with one line of code.
        System.out.println("inputFiles");

        Example example = new Example();
        example.test();
//        int exitCode = new CommandLine(new Example()).execute(args);
//        System.exit(exitCode);
    }
}